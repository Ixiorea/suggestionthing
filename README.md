## ixi's code resources (yay)

free to use

## suggestionbox
Thanks for sadgrl.online for the script, I modified it a little so it works for rocket.chat!

Here's a template for those who wants to use it.
~~~javascript
// change the placeholders!
document.getElementById('send').onclick = function() {
    var request1 = new XMLHttpRequest();
    request1.open("POST", "URL_HERE");
    request1.setRequestHeader('Content-type', 'application/json');
    var id1 = "{CHANGE}: " + document.getElementById("id1").value + "\n";
    var id2 = "{CHANGE}: " + document.getElementById("id2").value;
    var msg1 = id1 + id2;
    request1.send(JSON.stringify({'text': msg1})); // don't modify the 'text' thing or it wont work.
    alert('Submitted :D'); // can be anything else.
    document.getElementById("ID_HERE", "ID_HERE").value = '';
    // please change the placeholders if it breaks.
};

// if you need to make a button for a different thing use below. otherwise don't.
document.getElementById('submit').onclick = function() {
    var request2 = new XMLHttpRequest();
    request2.open("POST", "URL_HERE");
    request2.setRequestHeader('Content-type', 'application/json');
    var id3 = "{CHANGE}: " + "\n" + document.getElementById("id3").value;
    var msg2 = id3;
    alert("Submitted :P");
    request_suggest.send(JSON.stringify({'text': msg2}));
    document.getElementById("ID_HERE").value = '';
    // don't forget to change the placeholders, for example 'id1' into 'suggestionbox'
};
~~~

Hope it helps!